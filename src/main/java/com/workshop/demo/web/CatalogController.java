package com.workshop.demo.web;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.workshop.demo.model.ItemBO;
import com.workshop.demo.model.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sergey on 3/16/16.
 */
@RestController
@RefreshScope
public class CatalogController {

    @Autowired
    private ItemRepository repository;


//    @Autowired
//    @LoadBalanced
//    private RestTemplate template;

    @Autowired
    Receiver receiver;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired(required = false)
    private ApplicationInstanceInfo applicationInstanceInfo;

    private static final String SESSION_KEY = "session_key";
    private String URL = "http://catalog.cf-dev.altoros.com/list";


    private final static Logger logger = LoggerFactory.getLogger(CatalogController.class);

    @RequestMapping("/debug")
    public String getDebugMessage(HttpServletRequest request){
        String response = "Debug: ";
        response += applicationInstanceInfo != null ? "Instance id: " + applicationInstanceInfo.getInstanceId() : "Not a cloud configuration";
        String name = (String)request.getSession().getAttribute(SESSION_KEY);
        response += "Previous item: " + name;
        logger.debug(response);
        return response;
    }

    @RequestMapping("/add/{name}")
    public String addItem(HttpServletRequest request, @PathVariable String name){
        ItemBO item = new ItemBO();
        item.setName(name);
        repository.save(item);

        request.getSession().setAttribute(SESSION_KEY, item.getName());

        return "New item created: " + name;
    }

    @RequestMapping("/list")
    public @ResponseBody Iterable<ItemBO> getAllItems(){
        return repository.findAll();
    }

    @RequestMapping("/addToQueue/{name}")
    public String addItemToQueue(HttpServletRequest request, @PathVariable String name){
        rabbitTemplate.convertAndSend(RabbitConfiguration.queueName, name);
        return "Message added to the queue";
    }



    @HystrixCommand(fallbackMethod = "getFallBack")
    @RequestMapping("/listProtected")
    public @ResponseBody ItemBO[] getListHystrix(){
        RestTemplate template = new RestTemplate();
        ResponseEntity<ItemBO[]> responseEntity = template.getForEntity(URL, ItemBO[].class);
        return responseEntity.getBody();
    }

    private ItemBO[] getFallBack(){
        ItemBO item = new ItemBO();
        item.setName("Predefined answer");
        return new ItemBO[]{item};
    }


}
