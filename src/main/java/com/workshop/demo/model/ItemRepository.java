package com.workshop.demo.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by sergey on 3/16/16.
 */
public interface ItemRepository extends CrudRepository<ItemBO, Integer>{
}
