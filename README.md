# Create Application Template

* In order to generate aplication's template open [http://start.spring.io](http://start.spring.io) web site
* Specify Group and Artefact names
* Select WEB dependency 
* Generate the project

Archive contains the source code for simple Spring Boot application. This app can be compiled and pushed to cloud foundry:

	mvn clean package
	cf push APP_NAME -b https://github.com/cloudfoundry/staticfile-buildpack.git

At this point application should be uploaded to Cloud Foundry and started automatically. 

# Configuration

**IMPORTANT**: the link to CF instance in this example set to "http://APP_NAME.cf-dev.altoros.com" please update it to correct path to your own CF instance !!!!!

Configuration can be moved to **manifest.yml** file. Application manifests tell `cf push` what to do with applications. This includes everything from how many instances to create and how much memory to allocate to what services applications should use.

An example of manifest.yml file:

	---
	memory: 512mb
	instances: 1
	buildpack: https://github.com/cloudfoundry/java-buildpack.git
	applications:
	- name: catalog
	  host: catalog
	  path: target/workshop-0.0.1-SNAPSHOT.jar

with this file in a root directory of the project you can use `cf push` without extra parameters provided via command line

Configuration can be updated in runtime with the help of spring cloud configuration. In order to use spring cloud you have to add following dependencies to pom.xml

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-server</artifactId>
			<version>1.0.4.RELEASE</version>
		</dependency>

and enable servie using annotation `@EnableConfigServer` in class WorkshopApplication.

######Demo
* add log message in debug mode (see CatalogController.getDebugMessage )
* Set default log level to INFO level `logging.level.com.workshop.demo=INFO`
* deploy application to CloudFoundry
* Load page http://catalog.cf-dev.altoros.com/debug
* check that there is no log message 
* Change debug level using command `curl -XPOST http://catalog.cf-dev.altoros.com/env -d logging.level.com.workshop.demo=DEBUG`
* reload 'debug' page again and check if log message added to the log file

#Services
Create MySQL service instance using `cf create-service` command. add following dependencies to pom.xml

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>
		
Create **ItemBO** and **ItemRepository** the same way as it done in this example. Update **CatalogController** and create methods to "add" and "list" items.

#Session manager
By default session is stored locally in application instance. We can keep in remotely in Redis, in this case following changes to configuration are required:

Session and redis dependencies:

		<dependency>
			<groupId>org.springframework.session</groupId>
			<artifactId>spring-session</artifactId>
			<version>1.1.0.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-redis</artifactId>
		</dependency>
		
And ApplicationInstanceInfo

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-spring-service-connector</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-cloudfoundry-connector</artifactId>
		</dependency>
		
new class:

	@EnableRedisHttpSession
	public class HttpSessionConfig {}
	
In order to disable CloudFoundry sticky session we have to give custom name cookie:

	server.session.cookie.name = MYSESSIONID

In order to compile locally the access to Redis is required:

	git clone https://github.com/mdevilliers/vagrant-redis
	cd vagrant-redis
	vagrant up

and add information about Redis to `application.properties` file:

	spring.redis.database=0
	spring.redis.host=localhost
	spring.redis.port=6379