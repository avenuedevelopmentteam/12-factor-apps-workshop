package com.workshop.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
//import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableConfigServer
@EnableHystrix
//@EnableEurekaServer
//@EnableDiscoveryClient
public class WorkshopApplication {
	public static void main(String[] args) {
		SpringApplication.run(WorkshopApplication.class, args);
	}

}
